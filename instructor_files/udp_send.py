import socket
import sys
import struct
import time
import random

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sdr_addr = ('localhost', 6001)

# first, make the preamble
preamble = struct.pack('<8B',0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA)
   
# make the start start bit
start = struct.pack('<1B',0xF0)

# start the UDP send loop
while True:
   print "STARTING REQUEST LOOP"
   # make the different message data
   # temp, pressure, light
   tempVal = random.randrange(105, 115)
   presVal = random.randrange(100,200)
   lightVal = random.randrange(1, 50)
   print str(tempVal)+" "+str(presVal)+" "+str(lightVal)

   # send a control packet, ask for light
   control = struct.pack('<9B',0x02,0x01,ord("C"),ord('T'),ord('L'),0x03,0xFF,0x00,0x00)
   message = preamble+start+control
   sock.sendto(message, sdr_addr)

   # sleep 1
   time.sleep(1.5)

   # send a light packet, reply
   light = struct.pack('<10B',0x01,0x02,ord("L"),ord('G'),ord('T'),lightVal,0x03,0xFF,0x00,0x00)
   message = preamble+start+light
   sock.sendto(message, sdr_addr)

   # sleep 3
   time.sleep(5)

   # send a control packet, ask for tmp
   control = struct.pack('<9B',0x03,0x01,ord("C"),ord('T'),ord('L'),0x04,0xFF,0x00,0x00)
   message = preamble+start+control
   sock.sendto(message, sdr_addr)

   # sleep 1
   time.sleep(1.5)

   # send a temp packet, reply
   temp = struct.pack('<10B',0x01,0x03,ord("T"),ord('M'),ord('P'),tempVal,0x04,0xFF,0x00,0x00)
   message = preamble+start+temp
   sock.sendto(message, sdr_addr)

   # sleep 3
   time.sleep(5)

   # send a control packet, ask for pres
   control = struct.pack('<9B',0x04,0x01,ord("C"),ord('T'),ord('L'),0x05,0xFF,0x00,0x00)
   message = preamble+start+control
   sock.sendto(message, sdr_addr)

   # sleep 1
   time.sleep(1.5)

   # send a pres packet, reply
   light = struct.pack('<10B',0x01,0x04,ord("P"),ord('R'),ord('S'),presVal,0x05,0xFF,0x00,0x00)
   message = preamble+start+light
   sock.sendto(message, sdr_addr)

   print "SENT\n"
   # sleep3
   time.sleep(5)
   


