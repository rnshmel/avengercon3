import socket
import sys
import struct

print("SENDING FALSE PACKET\n")

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sdr_addr = ('localhost', 6001)

# first, make the preamble
preamble = struct.pack('<8B',0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA)

# make the start start bit
start = struct.pack('<1B',0xF0)

# HINT: the ord("char") will convert an ASCII character to a byte
# send a temp packet, spoofed
rxID = # control node ID
txID = # sensor node ID
val1 = # first character of sensor name
val2 = # second character of sensor name
val3 = # third character of sensor name
tempVal = # temp value to spoof
checksum = # checksum
temp = struct.pack('<10B',rxID,txID,val1,val2,val3,tempVal,checksum,0xFF,0x00,0x00)
message = preamble+start+temp
sock.sendto(message, sdr_addr)
print("SENT\n")

   


