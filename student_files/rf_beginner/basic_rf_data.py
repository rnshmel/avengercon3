import numpy as np  
import scipy.signal as signal
import scipy.fftpack as fftpack
import matplotlib
import matplotlib.pyplot as plt
import sys
from collections import Counter
import socket
import struct
import time
import random

#======================================================#
# PACKET DEMOD CODE - DO NOT MODIFY
#======================================================#

myfile = open("pack_data.txt","w+")
myfile.write("STARTING LOG\n\n")

# global vars
samp_rate = 10000

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
grc_addr = ('localhost', 6005)
sock.bind((grc_addr))

while True:
   data, addr = sock.recvfrom(9200) # buffer size is 9200 bytes
   if len(data) > 0:

      # read in the signal as a complex number from the data packet
      sig = np.fromstring(data, dtype="float32")
      sig = sig.astype("float16")
      
      # iterate through the signal and clean it up
      # val > 0 = 1	val < 0 = -1
      for i in range(len(sig)):
         if sig[i] > 0:
            sig[i] = 1
         else:
            sig[i] = 0

      # do a simple symbol size estimation
      # find the sample-length of a single bit of the preamble
      start_val = sig[0] # save start val
      count = 0
      t1 = 0
      t2 = 0
      not_done = True
      # iterate through bit stream, looking where start_val switches
      while not_done:
         if sig[count] != start_val:
            t1 = count
            not_done = False
         count = count + 1
      # iterate through bit stream, looking where start_val resumes
      not_done = True
      count = t1
      while not_done:
         if sig[count] == start_val:
            t2 = count
            not_done = False
         count = count + 1
      samp_len = t2 - t1
      #print("probable sym size (samples): "+str(samp_len))
      baud_rate = samp_rate/samp_len
      #print("probable baud rate: "+str(int(baud_rate)))

      # now make a sample preamble to try and corrilate to
      preamble_sequence = [1.0,0.0,1.0,0.0,1.0,0.0,1.0,0.0]
      test_preamble = np.empty(0, dtype="float16")
      for i in preamble_sequence:
         chunk = np.full(samp_len, i, dtype="float16")
         test_preamble = np.append(test_preamble,chunk)
     
      # call np.correlate to compute cross corr function of signal and preamble
      corr = abs(np.correlate(sig, test_preamble))
      corr_val = np.amax(corr)
     
      # if corr val met...
      if corr_val > 40:
         # conduct a simple clock recovery on signal (if valid)
         clock_rec_sig = []
         temp = sig[0]
         samp_count = 0
         for i in range(0,len(sig)-1):
            if sig[i] != temp:
               for j in range(0,round(samp_count/samp_len)):
                  clock_rec_sig.append(int(temp))
               temp = sig[i]
               samp_count = 0
            samp_count = samp_count + 1
         #print(clock_rec_sig)
         # strip the preamble
         not_done = True
         count = 5
         while not_done:
            if clock_rec_sig[count+1] == clock_rec_sig[count]:
               not_done = False
            else:
               count = count + 1
         clock_rec_sig = clock_rec_sig[count:]
         clock_rec_sig = "".join(str(x) for x in clock_rec_sig)
         raw_data = clock_rec_sig
         #print(clock_rec_sig)
         # convert to hex
         n = 8
         clock_rec_sig = [clock_rec_sig[i:i+n] for i in range(0, len(clock_rec_sig), n)]
         sig_data = []
         for i in clock_rec_sig:
            sig_data.append(hex(int(i,2))[2:])
         sig_data.append('0')
         sig_data.append('0')
         #print(sig_data)
         #print(bytes.fromhex('54').decode('ASCII'))

         #======================================================#
         # END PACKET DEMOD CODE - FEEL FREE TO MODIFY PRINT CODE
         #======================================================#
         #
         # Some helpful code:
         # to convert a hex string into decimal use: int(str,16)
         # to convert a hex string into ASCII use: bytes.fromhex('str').decode('ASCII')

         print("====================================================")
         print("DATA BURST DETECTED")
         print("--- extracted hex data ---")
         print("byte 0: " + sig_data[0])
         print("byte 1: " + sig_data[1])
         print("byte 2: " + sig_data[2])
         print("byte 3: " + sig_data[3])
         print("byte 4: " + sig_data[4])
         print("byte 5: " + sig_data[5])
         print("byte 6: " + sig_data[6])
         print("byte 7: " + sig_data[7])
         print("byte 8: " + sig_data[8])
         print("====================================================\n")

         # write a file for ease of use
         myfile.write("====================================================\n")
         myfile.write("DATA BURST DETECTED\n")
         myfile.write("--- extracted hex data ---\n")
         myfile.write(str(sig_data) + "\n")
         myfile.write("====================================================\n\n")




